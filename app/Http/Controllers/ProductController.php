<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Products;
use Symfony\Component\HttpFoundation\Response;
use Auth;
use DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

class ProductController extends Controller
{
    public function addproduct(Request $request) {
        try {
            if (! $token = JWTAuth::parseToken()) {
                return response()->json(['status'=>true]);
            }
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json(['status'=>false]);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->json(['status'=>false]);
            } else if ( $e instanceof \Tymon\JWTAuth\Exceptions\JWTException) {
                return response()->json(['status'=>false]);
            }else{
                return response()->json(['status'=>false]);
            }
        }

        $item = Products::create([
            'productname' => $request->productname,
            'price' => $request->price,
            'qty' => $request->qty
        ]);

        if($item) {
            return response()->json([$item,'status'=>true]);
        }else {
            return response()->json(['status'=>false]);
        }
    }

    public function delete($id) {
        $delete = Products::find($id)->forceDelete();
        return response()->json([$delete,'status'=>'Success Delete']);
    }

    public function update(Request $request, $id) {
        $update = Products::find($id)->update([
            'productname'=>$request->productname,
            'price'=>$request->price,
            'qty'=>$request->qty,
            'created_at'=>Carbon::now()
        ]);

        return response()->json([$update,'status'=>true]);
    }

    public function viewproduct() {
        $item = DB::table('products')->select('productname','price','qty')->get();
        return response()->json([$item,'status'=>true]);
    }
}
