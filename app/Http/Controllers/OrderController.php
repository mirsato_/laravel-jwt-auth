<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Order;
use App\Models\Products;

class OrderController extends Controller
{
    public function store(Request $request)
    {

        $order = Order::create($request->all());

        $products = $request->input('products', []);
        $quantities = $request->input('quantities', []);
        for ($product=0; $product < count($products); $product++) {
            if ($products[$product] != '') {
                $order->products()->attach($products[$product], ['quantity' => $quantities[$product]]);
            }
        }
        
        // $order = Order::create([
        //     'productname' => $request->productname,
        //     'quantities' => $request->quantities
        // ]);

        // $products = $request->input('productname', []);
        // $quantities = $request->input('quantities', []);

        // for ($product=0; $product < count($products); $product++) {
        //     if ($products[$product] != '') {
        //         $order->products()->attach($products[$product], ['quantity' => $quantities[$product]]);
        //     }
        // }


    }
}
