<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


//User Auth

Route::group(['middleware'=>'api','prefix'=>'auth'],function($router){
    Route::post('/register',[AuthController::class,'register']);
    Route::post('/login',[AuthController::class,'login']);
    Route::get('/profile',[AuthController::class,'profile']);
    Route::post('/logout',[AuthController::class,'logout']);
    Route::post('/store',[OrderController::class,'store']);
    Route::get('/viewproduct',[ProductController::class,'viewproduct']);
});

Route::post('adminregister', [App\Http\Controllers\AdminController::class,'adminregister'])->name('adminregister');
Route::post('adminlog', [App\Http\Controllers\AdminController::class,'adminlog'])->name('adminlog');
Route::view('adminlog','Admin/login')->name('adminlog');

//Admin Auth

Route::group([

    ['middleware' => 'auth:admin-api']

], function () {
    Route::post('logout', [App\Http\Controllers\AdminController::class,'logout'])->name('logout');
    Route::get('me', [App\Http\Controllers\AdminController::class,'me'])->name('me');
    Route::post('addproduct', [App\Http\Controllers\ProductController::class,'addproduct'])->name('addproduct');
    Route::post('delete/{id}', [App\Http\Controllers\ProductController::class,'delete'])->name('delete');
    Route::post('update/{id}', [App\Http\Controllers\ProductController::class,'update'])->name('update');
});
